// console.log("Hello, B190");

// // IF STATEMENT
//     // Syntax: 
//     // if(condition){
//     //     statement/code block
//     // }

let numA = -1;

if (numA < 0) {
    console.log("Hello!");
}

// console.log(numA < 0);

if (numA > 0) {
    console.log("This statement will not be printed");
}

let city = "New York";

if (city == "New York!") {
    console.log("Welcome to New York City!");
}
// "else if" clause
// previous condition is FALSE ; specified condition is TRUE
// is optional or capture additional condition

let numB = -3;

if (numA > 0) {
    console.log("Hello!");  // false
}   else if (numB > 0) {
    console.log("World");
}   // output: World

city

if(city === "New York"){
    console.log("New York");
} else if (city === "Tokyo"){
    console.log("Tokyo");
}

if (numA > 0) {
    console.log("Hello");
} else if (numB === 0) {
    console.log("World");
} else {
    console.log("Again");
}

// Another example
// let age = 20;
let age = prompt("Enter your age:")

if (age <= 18) {
    console.log("Not allowed to drink!");
} else {
    console.log("Matanda ka na, shot na!");
}

// 



// function height (yourHeight) {
// if (yourHeight < 150) {
//     console.log("Did not passed the minimum height requirement.");
// } else {
//     console.log("Passed the minumum height requirement");
// }
// };
// let checkHeight = height (prompt("Enter your height"))

// solution 1
let height = 160;

if (height < 150) {
    console.log("Did not passed!");
} else {
    console.log("Passed the min. height requirement.");
}

// solution 2
function heightReq(h){

    if (h < 150) {
        console.log("Did not pass");
    } else {
        console.log("Passed");
    }
}

heightReq(140);
heightReq(140);
heightReq(140);
// heightReq("Renz");

let message = "No message.";
console.log(message);

function determineTyphoonIntensity (windSpeed) {

    if (windSpeed < 30) {
        return "Not a typhoon yet"
    } else if (windSpeed <= 61) {
        return "Tropical depression detected."
    } else if (windSpeed >= 62 && windSpeed <= 88) {
        return "Tropical storm detected."
    } else if (windSpeed >= 89 && windSpeed <= 117) {
        return "Sever tropical storm detected."
    } else {
        return "Typhoon detected."
    }
}
// determineTyphoonIntensity(69);

message = determineTyphoonIntensity(69);
console.log(message);

// console.log(determineTyphoonIntensity(69));

if (message == "Tropical storm detected.") {
    console.warn(message);
}

// Truthy and Falsy
// falsy - false, 0, -0, "", null, undefined, Nan

if (true) {
    console.log("Truthy!");
}

if(1) {
    console.log("Truthy!");
}
if([]) {
    console.log("Truthy!");
}

// Falsy Examples
if (false) {
    console.log("Falsy!");
}
if(0) {
    console.log("Falsy!");
}
if(undefined) {
    console.log("Falsy");
}

// Conditional (Ternary) Operator - alternative to if else
/* takes three operands:
1. condition
2. expression to execute if the condition is truthy
3. expression to execute if the condition is falsy

    Syntax:
    (condition) : ifTrue : ifFalse
*/

// Single Statement Execution
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge() {
    name = "John";
    return "You are of the legal age limit"
}

function isUnderAge() {
    name = "Jane";
    return "You are under the age limit"
}

let age = parseInt(prompt("What is your age?"));   //string to a number in a prompt
console.log(age);
console.log(typeof age);
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " +legalAge + ", " + name);

// Switch Statement
/*
    Syntax:
        switch (expression) {
            case value :
                statement
                break;
            default:
                statement:
                break;
        }
*/

let day = prompt("What day of the week is it today?").toLowerCase();   //toLowerCase - not case sensitive
console.log(day);

        switch(day) {
            case "monday":
                console.log("The color of the day is red");
                break;
            case "tuesday":
                console.log("The color of the day is orange");
                break;
            case "wednesday":
                console.log("The color of the day is yellow");
                break;
             case "thursday":
                console.log("The color of the day is green");
                break;
            case "friday":
                console.log("The color of the day is blue");
                break;
            case "saturday" || "satarday":
                console.log("The color of the day is indigo");
                break;
            case "sunday":
                console.log("The color of the day is violet");
                break;
            // case "saturday" || "satarday":
            //     console.log("The color of the day is indigo");
            //     break;
            default:
                console.log("Please input a valid day!");
                    break;
        }

        // Try-Catch-Finally Statement
        function showIntensityAlert(windSpeed) {
            try {
                alerat(determineTyphoonIntensity(windSpeed))  //try to run/execute block
            }
            catch (error) {
                //will catch errors within the try statement
                console.log(error);  //object -
                console.warn(error.message); //object.property
            }
            finally {
                //will continue to execute code regardless of success or failure of code execution in the try block to handle/resolve errors
                alert("Intensity updates will show new alert.")
            } //even with error, still executed
        }
        showIntensityAlert(56)


        operator &&
        >=
        <=


         // function login() {
    //     // let username = prompt("Enter your username:");
    //     if ((username == null || username == "") || (password == null || password == "") || (role == null || role == "")) {
    //         alert("Input must not be empty.");
    //         // return;
    //     }
